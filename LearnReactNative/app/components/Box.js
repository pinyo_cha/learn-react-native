import React, { Component } from 'react'
import {
    View,
    StyleSheet
} from 'react-native'

export class Box extends Component {
    render() {
        return (
            <View>
                <View style={styles.box1}/>
                <View style={styles.box2}/>
                <View style={styles.box3}/>
                <View style={styles.box4}/>
                <View style={styles.box5}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    box1: {
        width: 50,
        height: 50,
        backgroundColor: '#e67e22'
    },

    box2: {
        width: 60,
        height: 60,
        backgroundColor: '#e74c3c'
    },

    box3: {
        width: 70,
        height: 70,
        backgroundColor: '#ecf0f1'
    },

    box4: {
        width: 80, 
        height: 80,
        backgroundColor: '#95a5a6'
    },

    box5: {
        width: 90,
        height: 90,
        backgroundColor: '#2c3e50'
    }
})