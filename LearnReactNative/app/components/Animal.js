import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Text,
    Image
} from 'react-native'

export class Animal extends Component {
    render() {
        let pic = { uri: 'https://media.giphy.com/media/3o8doNAGKZXsrsgzW8/giphy.gif'}
        let dog = require('../assets/images/dogCute.gif')
        return (
            <View style={styles.contianer}>
                <Text>Title : {this.props.title}</Text>
                <Image source={pic} style={styles.image} />
                <Image source={dog} style={styles.image}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    contianer: {
        marginTop: 50,
        width: 300,
        // height: 600,
        backgroundColor: 'green',
        justifyContent: 'center',
        alignItems: 'center'
    },

    image: {
        width: 250,
        height: 250
    }
})