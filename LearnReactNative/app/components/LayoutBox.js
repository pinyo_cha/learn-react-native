import React, { Component } from 'react'
import { 
    View,
    StyleSheet
 } from 'react-native'

 export class LayoutBox extends Component {
     render() {
         return (
             <View style={styles.container}>
                <View style={styles.box1}/>
                <View style={styles.box2}/>
                <View style={styles.box3}/>
                <View style={styles.box4}/>
                <View style={styles.box5}/>
             </View>
         )
     }
 }

 const styles = StyleSheet.create({
     container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#576574'
     },

     box1: {
         width: 50,
         height: 50,
         backgroundColor: '#ff9ff3'
     },

     box2: {
         width: 50,
         height: 50,
         backgroundColor: '#feca57'
     },

     box3: {
         width: 50,
         height: 50,
         backgroundColor: '#ff6b6b'
     },

     box4: {
         width: 50,
         height: 50,
         backgroundColor: '#48dbfb'
     },

     box5: {
         width: 50,
         height: 50,
         backgroundColor: '#1dd1a1'
     }
 })