import React, { Component } from 'react'
import {
    View,
    StyleSheet,
} from 'react-native'

export class SharedArea extends Component {
    render() {
        return (
            <View style={{flex: 1}}>
                <View style={styles.area1}/>
                <View style={styles.area2}/>
                <View style={styles.area3}/>
                <View style={styles.area4}/>
                <View style={styles.area5}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    area1: {
        flex: 1,
        backgroundColor: '#1abc9c'
    },

    area2: {
        flex: 2,
        backgroundColor: '#2ecc71'
    },

    area3: {
        flex: 3,
        backgroundColor: '#3498db'
    },

    area4: {
        flex: 4,
        backgroundColor: '#9b59b6'
    },

    area5: {
        flex: 5,
        backgroundColor: '#34495e'
    }
})