import React, { Component } from 'react'
import { Text, View, FlatList } from 'react-native'

export class MovieList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true
        }
    }

    componentDidMount() {
        this.getMovieFromService()
    }

    getMovieFromService () {
        const url = 'https://facebook.github.io/react-native/movies.json'
        
        fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                isLoading: false,
                dataSource: responseJson.movies
            })
        })
        .catch((error) =>{
            console.error('get movie error: ' + error)
        })

    }

    render() {
        return (
            <View style={{flex: 1, paddingTop: 50, alignItems: 'center'}}>
                <FlatList
                    data={this.state.dataSource}
                    renderItem={ ({ item, index }) => <Text style={{fontSize: 20}}>{index} : {item.title}, {item.releaseYear}</Text>}
                    keyExtractor={({id}, index) => id}
                />
            </View>
        )
    }
}
