import React, { Component } from 'react'
import { Text, View, FlatList, StyleSheet } from 'react-native'

export class TextListView extends Component {
  render() {
    return (
      <View style={styles.container}>
          <FlatList
            data={[
                {title: 'The Pinyo', color: 'yellow'},
                {title: 'Tle', color: 'pink'},
                {title: 'Tony', color: 'yellow'},
                {title: 'Benz', color: 'pink'},
                {title: 'Yen', color: 'yellow'},
                {title: 'Ved', color: 'pink'},
                {title: 'Nop', color: 'yellow'},
                {title: 'Shakty', color: 'pink'},
                {title: 'Meng', color: 'yellow'},
                {title: 'Hiro', color: 'pink'}
            ]}
            renderItem={({item, index}) => <Text style={[styles.item, {backgroundColor: item.color}]}>{item.title}</Text>}
          />
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 50,
        backgroundColor: 'red'
    },

    item: {
        fontSize: 20,
        height: 44,
        textAlign: 'center',
        flex: 1,
    },

    
})