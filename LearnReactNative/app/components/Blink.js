import React, { Component } from 'react'
import {
    View, 
    Text,
    StyleSheet,
} from 'react-native'
import PropTypes from 'prop-types'

export class Blink extends Component {
    constructor(props) {
        super(props)
        this.state = { isToggle: true }

        setInterval(() => {
            this.setState( previousState => (
                {
                    isToggle: !previousState.isToggle
                }     
            ))
        }, 500)
    }

    render() {
        return (
            <Text style={styles.title}>{this.state.isToggle ? this.props.first : this.props.second}</Text>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        color: 'red',
        fontWeight: 'bold',
        fontSize: 30
    }
})

Blink.propTypes = {
    first: PropTypes.string,
    second: PropTypes.string
}

Blink.defaultProps = {
    first: '+.+',
    second: '=.='
}