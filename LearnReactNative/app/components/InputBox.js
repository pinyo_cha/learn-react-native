import React, { Component } from 'react'
import { 
    View,
    TextInput,
    Text,
    StyleSheet,
    Button,
    TouchableHighlight,
    Alert,
 } from 'react-native'
 
 export class InputBox extends Component {
    constructor(props) {
        super(props)
        this.state = {
            text: ''
        }
    }
    
    onPressButton() {
        Alert.alert('You tapped the Button!')
    }

    onPressCustomButton() {
        Alert.alert('You tapped the Custom Button!')
    }
    
    render() {
        return (
            <View style={styles.container}>
                {/* Text Input */}
                <TextInput 
                    style={{height: 60, fontSize: 20}}
                    placeholder="Type here to translate."
                    onChangeText={ (text) => this.setState({text})}
                />
                <Text style={{fontSize: 60}}>
                    {this.state.text.split(' ').map((word) => word && '😘').join(' ')}
                </Text>

                {/* Button */}
                <View style={styles.button}>
                    <Button
                        style={{backgroundColor: 'blue'}}
                        onPress={this.onPressButton}
                        title='Hit Me'
                    />
                </View>

                {/* Custom Button */}
                <TouchableHighlight onPress={this.onPressCustomButton} underlayColor='white'>
                    <View style={styles.customButton}>
                        <Text style={{color: 'white'}}>Custom Button</Text>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }
 }

 const styles = StyleSheet.create({
     container: {
     }, 

     button: {
        padding: 20
     },

     customButton: {
         width: 260,
         height: 50,
         justifyContent: 'center',
         alignItems: 'center',
         backgroundColor: '#0abde3'
     }
 })