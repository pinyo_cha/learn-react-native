
import React, { Component } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Image,
    ScrollView,
    Button,
    NativeModules
} from 'react-native'
import { Animal } from './components/Animal.js'
import { Blink } from './components/Blink.js'
import { Box } from './components/Box.js'
import { SharedArea } from './components/SharedArea.js'
import { LayoutBox } from './components/LayoutBox.js'
import { InputBox } from './components/InputBox.js'
import {TextListView} from './components//TextListView.js'
import {MovieList} from './components/MovieList.js'

const { PhotoAlbumManager } = NativeModules

export default class Main extends Component {
    onPressDismissButton = () => {
        console.log("[Class]Main, root viewTag: " + this.props.viewTag)
        console.log("[Bridge] PhotoAlbumManager constant: ", PhotoAlbumManager.userId)
        PhotoAlbumManager.presentSomething(this.props.viewTag)
    }

    render() {
        return (
            <View style={styles.container2}>
                <ScrollView maximumZoomScale={100} horizontal={false} showsVerticalScrollIndicator={false}>              
                    <Animal title='Chicken'/>
                    <Button 
                        title='Call Native'
                        onPress={this.onPressDismissButton}
                    />
                    <Text>I'm Asender.</Text>
                    <Blink first='O.O' second='-.-'/>
                    <Blink first='Hello' second='Who are you?'/>
                    <Text>{'Props 1: ' + this.props.title1}</Text>
                    <Text>{'Props 2: ' + this.props.title2}</Text>
                    <Text>{'Props 3: ' + this.props.title3}</Text>
                    <Box/> 
                    <SharedArea/> 
                    <LayoutBox/>
                    <InputBox/>
                </ScrollView> 
                {/* <TextListView/> */}
                {/* <MovieList/>     */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'yellow'
    },

    container2: {
        flex: 1
    }
})