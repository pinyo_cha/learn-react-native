##Basic of React Native

example basic of react native, step by step

#### Quick Start

0. setup environment for develop
    - install Homebrew  
        ```
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
        ```
  
  
    - install node package manager
        ```
        brew install node
        ```
        and
        ```
        brew install watchman  
        ```
        
        
    - install react native
        ```
        npm install -g react-native-cli
        ```
        
1. Clone repo 
2. open project directory and open xcode workspace project, *(iOS Only)*
3. setup provisioning profile, *(iOS Only)*
4. open terminal and cd to project directory
5. command ```npm install```
6. run project ```react-native run-ios``` for iOS
7. run project ```react-native run-android``` for Android



####Step by Step
1. init project, *(tag: v0.1)*
2. creating components, style, props, *(tag: v0.2)*
3. how to custom text component, *(tag: v0.3)*
4. how to custom view by use width & height, *(tag: v0.4)*
5. how to use state, *(tag: v0.5)*
6. using flex, for control layout, *(tag: v0.6)*
7. Layout Box, using flexDirection, justifyContent, alignItems, etc.. , *(tag: v0.7)*
8. creating text field, *(tag: v0.8)*
9. creating button, custom button with gesture, *(tag: v0.9)*
10. using scrollView, *(tag: v0.10)*
11. using FlatList (likes listView, tableView), *(tag: v0.11)*
12. using fetch to call service, *(tag: v0.12)*
13. using propTypes for checking , *(tag: v0.13)*
14. using default props , *(tag: v0.14)*
15. create shortcut script build bundle for iOS only(^^), *(tag: v0.15)*
16. how to integrate react native to native (iOS only), *(tag: v0.16)*
17. using image component with local image file, *(tag: v0.17)*
18. Coming soon. O.o


#### More detail
https://facebook.github.io/react-native/ 

## License
Power by Cybertron.